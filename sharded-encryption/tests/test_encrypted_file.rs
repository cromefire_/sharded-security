use sharded_encryption::encrypted_file::EncryptedFile;
use std::fs::{remove_file, OpenOptions};
use std::io::{Read, Seek, SeekFrom, Write};
use std::path::Path;

#[macro_use]
extern crate hex_literal;

#[test]
fn encrypt_new_file() {
    let test_file_path = Path::new("/tmp/test.txt.shard");
    if test_file_path.exists() {
        remove_file(test_file_path).expect("file to be removed");
    }
    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .append(true)
        .create_new(true)
        .open(test_file_path)
        .expect("file to be created");
    file.write(&[0u8; 16]).unwrap();
    let key = hex!("530f492b2a20f27006c9aed3de8595a53cc0c1bd9076e4ed3a9e64912bdf8a16b8688eae91cec7d9082d567b6df25c74df25c00ee57a31deb024addd860d3739");
    let mut enc_file = EncryptedFile::new(Box::new(file), key, 16);

    // Test write to empty file
    enc_file
        .write("Hello World!".as_bytes())
        .expect("to write data");

    // Simple read
    enc_file
        .seek(SeekFrom::Start(6))
        .expect("seek to be successful");
    let mut read = [0u8; 5];
    enc_file.read(&mut read).expect("bytes to be read");
    assert_eq!(read, "World".as_bytes());

    // Overwrite
    enc_file
        .seek(SeekFrom::Start(6))
        .expect("seek to be successful");
    enc_file.write("Shard".as_bytes()).expect("to write data");

    // Read new string
    enc_file.rewind().expect("seek to be successful");
    let mut read = [0u8; 12];
    enc_file.read(&mut read).expect("bytes to be read");
    assert_eq!(read, "Hello Shard!".as_bytes());
}
